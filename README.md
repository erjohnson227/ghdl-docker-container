# Docker GHDL Container

## Introduction 

This repositoy provides a simple docker container for GHDL devleopment. IT is babsed on the Ubuntu 18.04 image, and automatically installs necessary build tools then clones and compiles GHDL with the LLVM back-end. In addition, GTKWave is installed to allow for easy viewing of VCD files. 

### Why Compile GHDL From Source?

The version of GHDL available on the aptitude package manager does not use the LLVM backend and therefore cannot be used to generate VCD files for examination in GTKwave. 

## Prerequisites

### Microsoft Windows 

Currently, all of the convenience scripts only support microsoft windows, there's no reason why this docker container couldnt be run on linux, I just haven't had the time to write a `start-container.sh` script for linux. If on linux, the cloning and compilation of GHDL is quite simple and can be accomplished with the following commands: 

```sh
sudo apt install llvm zlibg zlibg-dev
git clone https://github.com/ghdl/ghdl
cd /ghdl && mkdir build
cd /build
../configure --with-llvm-config
make 
sudo make install
```

This docker container was focused on deployment to Windows, which generally doesn't lend itself to compiling software "simply" or "easily". 

### Install Docker

In order to run this Docker container, you will need docker. 

Docker can be found at the following link: 

[Docker Desktop (Windows x64)](https://desktop.docker.com/win/stable/Docker%20Desktop%20Installer.exe)


Follow the instructions to install docker, and ensure that the option for "Use WSL2" is set. 

Restart your computer and open docker, you will be prompted to install the WSL2 kernel, follow the link, download the kernel, and run the installer. Hit restart on the docker dialog with the error to restart docker. 

### Install VcXsrv

VcXsrv is an X11 server that plays nicely with Docker, this is optional but will allow you to run GtkWave from within the docker container. 

[VcXsrv Sourceforge Page](https://sourceforge.net/projects/vcxsrv/) 

Run the installer, then run "XLaunch" from the start menu. 

In the Xlaunch window, ensure the "Display Number" is 0. Press next, then press next again. Check "no access control". Press next, then finish. 

Congratulations, you now have X11 forwarding set up in windows. 

### SSH Key 
Windows should have an open-ssh server installed and running, if not, then google how to install it. 

Open command prompt and run `ssh-keygen` to generate an ssh key, accept the default values for key name and location. 

Make sure the SSH Agent picks up on the new key and is running, this is done by executing the command: 
`start-ssh-agent` in a command prompt or powershell terminal. 
