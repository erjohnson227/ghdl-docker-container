FROM ubuntu:18.04

ARG SSH_PUBKEY=""
# Update package catelog
RUN apt-get update

# Avoid tz-data issues
ENV TZ=America/Los_Angeles
ARG DEBIAN_FRONTEND=noninteractive

# Install necesary packages
RUN apt-get -y install vim make subversion git clang \
                zlib1g zlib1g-dev gcc build-essential \
                llvm gnat gtkwave ca-certificates socat \
                 openssh-server supervisor rpl pwgen

# Grab GHDL
RUN git clone https://github.com/ghdl/ghdl

# Build and install GHDL
RUN cd /ghdl && mkdir build && cd build && ../configure --with-llvm-config \ 
    && make && make install 

RUN echo root:ghdl | chpasswd

# Set up SSH
RUN mkdir /var/run/sshd
ADD sshd.conf /etc/supervisor/conf.d/sshd.conf
RUN sed -i "/#AuthorizedKeysFile/c\AuthorizedKeysFile \/root\/.ssh\/authorized_keys" /etc/ssh/sshd_config
RUN sed -i "/#LogLevel/c\LogLevel INFO" /etc/ssh/sshd_config
RUN ssh-keygen -A
RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
RUN mkdir /root/.ssh  && chown -R root:root /root/.ssh &&  \
    touch /root/.ssh/authorized_keys && chmod 700 -R /root/.ssh/ && echo "${SSH_PUBKEY}" > /root/.ssh/authorized_keys



EXPOSE 22

RUN service ssh start && bash
