@echo off

:GETOPTS
    if /I "%1" == "/b" set BUILD=1
    if /I "%1" == "/k" set KILL=1
    if /I "%1" == "/del-ssh-key" set DEL_SSH_KEY=1 && echo fuck you
    if /I "%1" == "/v" set MNT_POINT_SPECIFIED=1 & set MOUNT_POINT=%2 & shift 
    shift
if not "%1" == "" goto GETOPTS


@REM echo %BUILD%
@REM echo %KILL%
@REM echo %MNT_POINT_SPECIFIED%
@REM echo %MOUNT_POINT%

if not "%MNT_POINT_SPECIFIED%" == "" (
    for /f "delims=" %%i in ("%MOUNT_POINT%") do set "MNT_FOLDER=%%~nxi"

    rem Remove trailing spaces
    for /f "tokens=* delims= " %%A in ("%MOUNT_POINT%") do set MOUNT_POINT=%%A
)

echo %MNT_FOLDER%

rem User can specificy /b to force rebuild. 
if not "%KILL%"=="" GOTO KILL
if not "%BUILD%"=="" goto FORCEBUILD
echo Checking for existing GHDL docker image...

rem Cheeck for docker image named ghdl_docker
set output=
for /f "delims=" %%A in ('docker images ^| findstr ghdl_docker') do set "output=%%A"

rem Docker container not found, initiate build. 
if NOT "%output%" == "" (
    echo Existing GHDL docker image found, skipping build. ^
    (use /b switch to force build
) else (
    echo No GHDL docker image found, commencing build...
    goto BUILD
)

rem Run the docker container
:RUN
set RUN_CMD=docker run -ti --rm
if not "%MNT_POINT_SPECIFIED%" == "" (
   set RUN_CMD=%RUN_CMD% -v "%MOUNT_POINT:~1,-2%:/root/VHDL/%MOUNT_FOLDER%"
)
set RUN_CMD=%RUN_CMD% -e DISPLAY=host.docker.internal:0.0 --name ghdl_docker_instance -p 2022:22 -d ghdl_docker
rem echo %RUN_CMD%
%RUN_CMD%
docker exec ghdl_docker_instance service ssh start
ssh -p 2022 root@localhost
goto NICE_EXIT

:FORCEBUILD
echo "-b specified, forcing docker container build..."
goto BUILD

rem Force re-build the docker container
:BUILD
rem Check for SSH key so that we can copy this over to the docker image
if exist "%userprofile%\.ssh\id_rsa.pub" ( 
    rem Found RSA key, sending to docker image" 
    echo Found RSA keyfile, sending to docker image. 
    for /f "delims=" %%A in ('type "%userprofile%\.ssh\id_rsa.pub"') do set "pubkey=%%A"

    if not "%DEL_SSH_KEY%" == "" (
        echo "Option specified to delete existing SSH key in known_hosts file..."
        type %userprofile%\.ssh\known_hosts | findstr -v 2022 > %userprofile%\.ssh\known_hosts
    )
) else (
    echo No RSA keyfile found, passowrd auth required for ssh. 
)
set  BUILD_CMD=docker build -t ghdl_docker --build-arg SSH_PUBKEY="%pubkey%" ./ghdl-docker-container
rem echo %BUILD_CMD%
%BUILD_CMD%
echo Build complete, starting docker container... 
goto RUN

:KILL 
echo "Stopping and Removing container..."
docker stop ghdl_docker_instance
goto NICE_EXIT

rem Apparently environmental variables persist even after a batch file has stopped
rem running. This sucks, so we have to clean all of the variables we used up here
rem otherwise they will persist forever and screw things up in subsequent runs. 
:NICE_EXIT
set BUILD=
set KILL=
set MNT_FOLDER=
set MNT_POINT_SPECIFIED=
set MOUNT_POINT=
set DEL_SSH_KEY=
set RUN_CMD=
set BUILD_CMD=
exit /b 0
